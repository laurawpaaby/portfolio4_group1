'''
Utility functions. 
Code gratefully adopted and slightly modified from: https://github.com/laurabpaulsen/fMRI_analysis
'''
from pathlib import Path
import numpy as np
import pandas as pd
from nilearn import masking
import nibabel as nib
from nilearn.glm.first_level import FirstLevelModel

def cut_trial_label(trial_type):
    return trial_type[4:]

def load_prep_events(path): 
    """
    Loads the event tsv and modifies it to contain the events we want

    Parameters
    ----------
    path : Path
        Path to tsv file containing the events
    
    Returns
    -------
    event_df : pd.DataFrame
        Pandas dataframe containing the events
    """
    # load the data
    event_df = pd.read_csv(path, sep='\t')

    # get the data corresponding to the events and only keep the needed columns
    event_df = event_df.loc[:, ["onset", "duration", "trial_type"]]

    event_df["trial_type"] = event_df["trial_type"].apply(cut_trial_label)

    event_df = event_df.reset_index(drop = True)

    return event_df

def load_prep_confounds(path, confound_cols = ['trans_x', 'trans_y', 'trans_z', 'rot_x', 'rot_y', 'rot_z']):
    """
    Loads the confound tsv and modifies it to contain the events we want

    Parameters
    ----------
    path : Path
        Path to tsv file containing the events
    confound_cols : list of strings
        List of the column names that should be included in the confounds_df
    
    Returns
    -------
    confounds_df : pd.DataFrame
        Pandas dataframe containing the confounds
    """
    # load the confounds
    confounds_df = pd.read_csv(path, sep='\t')

    # choose specific columns
    confounds_df = confounds_df. loc[:, confound_cols]

    return confounds_df

def fit_first_level_subject(subject, bids_dir, runs = [1, 2, 3, 4, 5, 6], space = "MNI152NLin2009cAsym"):
    """

    Parameters
    ----------
    subject : str
        Subject identifier e.g. "0102".

    bids_dir : Path
        Path to the root of the bids directory.

    runs : list of int
        List of runs to load.

    space : str
        Name of the space of the data to load.
    
    Returns
    -------
    first_level_model : FirstLevelModel
        First level model fitted for one subject

    """
    
    bids_func_dir  = bids_dir / f"sub-{subject}" / "func"
    fprep_func_dir  = bids_dir / "derivatives" / f"sub-{subject}" / "func"
    

    # paths to functional preprocessed data for all runs
    fprep_func_paths = [fprep_func_dir / f"sub-{subject}_task-boldinnerspeech_run-{run}_echo-1_space-{space}_desc-preproc_bold.nii.gz" for run in runs]
    
    # paths to raw functional data for all runs
    raw_func_paths = [bids_func_dir / f"sub-{subject}_task-boldinnerspeech_run-{run}_echo-1_space-{space}_desc-preproc_bold.nii.gz" for run in runs]

    # prepare event files
    event_paths = [bids_func_dir / f"sub-{subject}_task-boldinnerspeech_run-{run}_events.tsv" for run in runs]
    events = [load_prep_events(path) for path in event_paths]

    # paths to confounds
    confounds_paths = [fprep_func_dir / f"sub-{subject}_task-boldinnerspeech_run-{run}_desc-confounds_timeseries.tsv" for run in runs]
    confounds = [load_prep_confounds(path) for path in confounds_paths]
    
    # prep masks
    mask_paths = [fprep_func_dir / f"sub-{subject}_task-boldinnerspeech_run-{run}_space-MNI152NLin2009cAsym_desc-brain_mask.nii.gz" for run in runs]
    masks = [nib.load(path) for path in mask_paths]

    # merge the masks
    mask_img = masking.intersect_masks(masks, threshold=0.8)

    # fit first level model
    first_level_model = FirstLevelModel(
        t_r=int(nib.load(fprep_func_paths[0]).header['pixdim'][4]), 
        mask_img = mask_img, 
        slice_time_ref = 0.5,
        hrf_model = "glover",
        verbose = 1
        )

    first_level_model.fit(fprep_func_paths, events, confounds)
    
    return first_level_model, events, confounds, mask_img